export function fetchData(data) {
  return new Promise(resolve => {
    setTimeout(resolve, 10, `async ${data}`);
  });
}

export function fetchDataWithError() {
  return new Promise((resolve, reject) => {
    setTimeout(reject, 10, 'error');
  });
}
